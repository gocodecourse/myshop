import React from "react";
import { useParams } from "react-router-dom";

import { useFetch } from "../hooks/useFetch";
import { PRODUCT_ID_URL } from "../config";
import Product from "../components/Product/Product";
import LoadingSpinner from "../components/LoadingSpinner/LoadingSpinner";

const ProductView = () => {
  const params = useParams();

  const {
    data: product,
    isLoading,
    error,
  } = useFetch(`${PRODUCT_ID_URL}/${params.productId}`);

  return isLoading ? (
    <LoadingSpinner />
  ) : error ? (
    <div>Error fetching data</div>
  ) : (
    <>
      {product.length && (
        <Product
          productId={product[0]._id}
          price={product[0].price}
          imageUrl={product[0].image}
          title={product[0].title}
          styleClasses={{
            productCard: "product-card-view",
            productImg: "product-img-view",
            productAbstract: "product-abstract-view",
            buttonsSection: "buttons-section-view",
            productDescription: "product-description-view",
          }}
          isProductView={false}
          description={product[0].description}
          category={product[0].category}
        />
      )}
    </>
  );
};

export default ProductView;
