import React, { useContext, useState } from "react";
import MiniDrawer from "../components/MiniDrawer/MiniDrawer";
import Products from "../components/Products/Products";
import CartModal from "../components/CartModal/CartModal";
import "./Home.css";
import StoreContext from "../StoreContext";

const Home = () => {
  const { openDrawer, setOpenDrawer } = useContext(StoreContext);
  const [openModal, setOpenModal] = useState(false);

  return (
    <>
      <div className="home">
        <MiniDrawer
          openModal={openModal}
          setOpenModal={setOpenModal}
          openDrawer={openDrawer}
          setOpenDrawer={setOpenDrawer}
        />
        <Products />
        <CartModal openModal={openModal} setOpenModal={setOpenModal} />
      </div>
    </>
  );
};

export default Home;
