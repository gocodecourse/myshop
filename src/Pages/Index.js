import Home from "./Home";
import About from "./About";
import ProductView from "./ProductView";
import NotFound from "./NotFound";
import EditProducts from "./EditProducts";

export { Home, About, ProductView, NotFound, EditProducts };
