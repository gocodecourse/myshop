import React, { useState } from "react";
import ProductsGrid from "../components/ProductsGrid/ProductsGrid";

const EditProducts = () => {
  const [currentRowId, setCurrentRowId] = useState("");
  const [action, setAction] = useState();

  return (
    <div style={{ height: "80vh" }}>
      <ProductsGrid
        currentRowId={currentRowId}
        setCurrentRowId={setCurrentRowId}
        action={action}
        setAction={setAction}
      />
    </div>
  );
};

export default EditProducts;
