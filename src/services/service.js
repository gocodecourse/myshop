let BASE_SERVER_URL = "";
console.log("process.env.NODE_ENV", process.env.NODE_ENV);
if (process.env.NODE_ENV === "production") {
  BASE_SERVER_URL = "https://myshopbe.onrender.com";
} else {
  BASE_SERVER_URL = "http://localhost:8000";
}

export { BASE_SERVER_URL };
