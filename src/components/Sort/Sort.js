import React from "react";

import { SORT_OPTIONS } from "../../config";

import Box from "@mui/material/Box";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";

const Sort = () => {
  const [sort, setSort] = React.useState("");

  const sortOptionsToDisplay = SORT_OPTIONS.map((option, index) => (
    <MenuItem key={`${option} ${index}`} value={option}>
      {option}
    </MenuItem>
  ));

  return (
    <Box sx={{ m: 1.5, width: 250 }}>
      <FormControl fullWidth>
        <InputLabel>Sort</InputLabel>
        <Select value={sort} label="Sort" onChange={(e) => setSort(e.target.value)}>
          {sortOptionsToDisplay}
        </Select>
      </FormControl>
    </Box>
  );
};

export default Sort;
