import React from "react";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Chip from "@mui/material/Chip";
import { Box } from "@mui/system";

import { ALL_ITEMS } from "../../config";

const MultipleSelectChip = ({
  categories,
  setCategory,
  chosenCategory,
  setChosenCategory,
}) => {
  const handleCategory = (e) => {
    setChosenCategory(e.target.value);
    setCategory(e.target.value);
  };

  return (
    <div>
      <FormControl sx={{ m: 1, width: 250 }}>
        <InputLabel>Filter</InputLabel>
        <Select
          multiple
          value={chosenCategory}
          onChange={handleCategory}
          input={<OutlinedInput label="Filter" />}
          renderValue={(selectedCategory) => (
            <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
              {chosenCategory.length ? (
                selectedCategory.map((category) => (
                  <Chip key={category} label={category} />
                ))
              ) : (
                <Chip key={ALL_ITEMS} label={ALL_ITEMS} />
              )}
            </Box>
          )}
        >
          {categories.map((category) => (
            <MenuItem key={category} value={category}>
              {category}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default MultipleSelectChip;
