import { Button, Card, CardContent, Typography } from "@mui/material";
import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";

import "./Product.css";
import StoreContext from "../../StoreContext";
import { addProductToCart, removeProductFromCart } from "./product.logic";

const Product = (props) => {
  const {
    productId,
    price,
    imageUrl,
    title,
    styleClasses,
    isProductView,
    description,
    category,
  } = props;
  const { cart, setCart } = useContext(StoreContext);
  const productViewNavigate = useNavigate();

  return (
    <Card raised className={styleClasses.productCard}>
      <img
        className={styleClasses.productImg}
        src={imageUrl}
        alt="img"
        onClick={
          isProductView
            ? () => {
                productViewNavigate(`product/${productId}`);
              }
            : ""
        }
      />
      <CardContent className="product-info">
        <CardContent className={styleClasses.productAbstract}>
          <Typography gutterBottom>{title}</Typography>
          <Typography gutterBottom>{price}$</Typography>
        </CardContent>
        {isProductView || (
          <CardContent className="product-description-view">
            <Typography align="left" variant="h6">
              About this product:
            </Typography>
            <Typography align="left" variant="h7">
              {category}
            </Typography>
            <Typography align="left" variantMapping="p">
              {description}
            </Typography>
          </CardContent>
        )}
      </CardContent>
      {!cart.has(productId) ? (
        <Button onClick={() => addProductToCart(props, cart, setCart)}>
          add to cart
        </Button>
      ) : (
        <div className={styleClasses.buttonsSection}>
          <Button onClick={() => removeProductFromCart(props, cart, setCart)}>-</Button>
          <span>{cart.get(productId).amount}</span>
          <Button onClick={() => addProductToCart(props, cart, setCart)}>+</Button>
        </div>
      )}
    </Card>
  );
};

export default Product;
