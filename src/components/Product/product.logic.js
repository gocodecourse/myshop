export const removeProductFromCart = (props, cart, setCart) => {
  const { productId, price, imageUrl, title } = props;
  if (cart.get(productId).amount !== 1) {
    setCart(
      (prevCart) =>
        new Map(
          prevCart.set(productId, {
            amount: prevCart.get(productId).amount - 1,
            price,
            imageUrl,
            title,
          })
        )
    );
  } else {
    setCart(() => {
      cart.delete(productId);
      return new Map(cart);
    });
  }
  // console.log(cart);
};

export const addProductToCart = (props, cart, setCart) => {
  const { productId, price, imageUrl, title } = props;
  if (cart.get(productId)) {
    setCart(
      (prevCart) =>
        new Map(
          prevCart.set(productId, {
            amount: prevCart.get(productId).amount + 1,
            price,
            imageUrl,
            title,
          })
        )
    );
  } else {
    setCart(
      new Map(
        cart.set(productId, {
          amount: 1,
          price,
          imageUrl,
          title,
        })
      )
    );
  }
  // console.log(cart);
};
