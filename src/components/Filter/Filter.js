import React, { useContext } from "react";

import "./Filter.css";
import { ALL_CATEGORIES_URL } from "../../config";
import { useFetch } from "../../hooks/useFetch";
import StoreContext from "../../StoreContext";
import RangeSlider from "../RangeSlider/RangeSlider";
import MultipleSelectChip from "../MultipleSelectChip/MultipleSelectChip";

const Filter = () => {
  const { data: categoriesFromServer } = useFetch(ALL_CATEGORIES_URL);
  const {
    productsFromServer,
    setCategory,
    priceRange,
    setPriceRange,
    chosenCategory,
    setChosenCategory,
  } = useContext(StoreContext);

  const maxPrice = productsFromServer.reduce(
    (maxPrice, product) => (product.price > maxPrice ? product.price : maxPrice),
    0
  );

  return (
    <div style={{ margin: "1.5em 0" }}>
      <MultipleSelectChip
        chosenCategory={chosenCategory}
        setChosenCategory={setChosenCategory}
        categories={categoriesFromServer}
        setCategory={setCategory}
      />
      <RangeSlider
        min={0}
        max={maxPrice}
        priceRange={priceRange}
        setPriceRange={setPriceRange}
      />
    </div>
  );
};

export default Filter;
