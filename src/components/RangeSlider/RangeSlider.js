import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";
import { MIN_DISTANCE } from "../../config";

const RangeSlider = ({ min, max, priceRange, setPriceRange }) => {
  const handleChange = (event, newValue, activeThumb) => {
    if (activeThumb === 0) {
      setPriceRange([Math.min(newValue[0], priceRange[1] - MIN_DISTANCE), priceRange[1]]);
    } else {
      setPriceRange([priceRange[0], Math.max(newValue[1], priceRange[0] + MIN_DISTANCE)]);
    }
  };

  return (
    <Box sx={{ mt: 7, ml: 3, width: 230 }}>
      <Slider
        min={min}
        max={max}
        value={priceRange}
        onChange={handleChange}
        valueLabelDisplay="on"
      />
    </Box>
  );
};

export default RangeSlider;
