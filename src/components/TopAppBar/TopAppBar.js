import { AppBar, IconButton, Toolbar, Typography } from "@mui/material";
import React from "react";
import MenuIcon from "@mui/icons-material/Menu";
import TopNavBar from "../TopNavBar/TopNavBar";

const TopAppBar = ({ openDrawer, setOpenDrawer, children }) => {
  return (
    <>
      <AppBar open={openDrawer} sx={{ height: 70 }}>
        <Toolbar>
          <IconButton
            onClick={() => setOpenDrawer(!openDrawer)}
            color="inherit"
            edge="start"
            sx={{ marginRight: 5 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h4">Not me Not you Shop</Typography>
          <TopNavBar />
        </Toolbar>
      </AppBar>
      <div style={{ marginTop: "6em" }}>{children}</div>
    </>
  );
};

export default TopAppBar;
