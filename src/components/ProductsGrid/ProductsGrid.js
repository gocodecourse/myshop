import React, { useContext } from "react";
import Box from "@mui/material/Box";
import { DataGrid } from "@mui/x-data-grid";
import StoreContext from "../../StoreContext";
import { Button } from "@mui/material";
import ActionButtonsGrid from "./ActionButtonsGrid";
import { handleAction } from "./ProductsGrid.logic";

const ProductsGrid = ({ currentRowId, setCurrentRowId, action, setAction }) => {
  const { productsFromServer } = useContext(StoreContext);
  // const [selectedRow, setSelectedRow] = React.useState([]);

  const columns = [
    {
      field: "id",
      headerName: "ID",
      width: 210,
      type: "string",
      editable: false,
    },
    {
      field: "title",
      headerName: "Title",
      width: 210,
      type: "string",
      editable: true,
    },
    {
      field: "price",
      headerName: "Price",
      width: 105,
      type: "number",
      editable: true,
    },
    {
      field: "description",
      headerName: "Description",
      width: 500,
      type: "string",
      editable: true,
    },
    {
      field: "image",
      headerName: "Image",
      width: 200,
      type: "string",
      editable: true,
    },
    {
      field: "category",
      headerName: "Category",
      width: 130,
      type: "string",
      editable: true,
    },
    {
      field: "action",
      headerName: "Action",
      width: 120,
      renderCell: (params) => (
        <ActionButtonsGrid
          params={params}
          currentRowId={currentRowId}
          setCurrentRowId={setCurrentRowId}
          action={action}
          setAction={setAction}
        />
      ),
    },
  ];

  const rowsToDisplay = productsFromServer.map((product) => {
    return {
      id: product["_id"],
      title: product["title"],
      price: product["price"],
      description: product["description"],
      image: product["image"],
      category: product["category"],
    };
  });

  // const onRowsSelectionHandler = (ids) => {
  //   const selectedRowsData = ids.map((id) => rows.find((row) => row.id === id));
  //   console.log(selectedRowsData);
  // };

  return (
    <Box sx={{ height: "100%", width: "100%" }}>
      <Button>add product</Button>
      <Button>delete all marked</Button>
      <DataGrid
        rows={rowsToDisplay}
        columns={columns}
        pageSize={12}
        rowsPerPageOptions={[12]}
        checkboxSelection
        disableSelectionOnClick
        // onSelectionModelChange={(ids) => onRowsSelectionHandler(ids)}
        // onSelectionModelChange={(rows) => setSelectedRow(rows)}
        experimentalFeatures={{ newEditingApi: true }}
        getRowHeight={() => "auto"}
        processRowUpdate={(newRow, oldRow) =>
          handleAction(action, currentRowId, newRow, oldRow)
        }
        onProcessRowUpdateError={(error) => alert(error)}
        editMode="row"
      />
    </Box>
  );
};

export default ProductsGrid;
