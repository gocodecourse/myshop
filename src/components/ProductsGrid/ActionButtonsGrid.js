import React from "react";
import { Tooltip } from "@mui/material";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/DeleteOutlined";
import SaveIcon from "@mui/icons-material/Save";
import CancelIcon from "@mui/icons-material/Close";
import Zoom from "@mui/material/Zoom";
import { deleteProductHandle } from "./ProductsGrid.logic";
import { CANCEL, SAVE } from "../../config";
import "./ActionButtonsGrid.css";

const ActionButtonsGrid = ({
  params,
  currentRowId,
  setCurrentRowId,
  action,
  setAction,
}) => {
  return (
    <>
      {params["api"]["getRowMode"](params["id"]) === "view" ? (
        <div className="icons-section">
          <Tooltip title="edit" placement="top" TransitionComponent={Zoom} arrow>
            <EditIcon
              onClick={() => {
                setCurrentRowId(params["id"]);
                params["api"]["startRowEditMode"]({ id: params["id"] });
              }}
            />
          </Tooltip>
          <Tooltip title="delete" placement="top" TransitionComponent={Zoom} arrow>
            <DeleteIcon
              onClick={() => {
                deleteProductHandle(params["id"]);
              }}
            />
          </Tooltip>
        </div>
      ) : (
        <div className="icons-section">
          <Tooltip title="save" placement="top" TransitionComponent={Zoom} arrow>
            <SaveIcon
              onClick={() => {
                setAction(SAVE);
                params["api"]["stopRowEditMode"]({ id: params["id"] });
              }}
            />
          </Tooltip>
          <Tooltip title="cancel" placement="top" TransitionComponent={Zoom} arrow>
            <CancelIcon
              onClick={() => {
                setAction(CANCEL);
                params["api"]["stopRowEditMode"]({ id: params["id"] });
              }}
            />
          </Tooltip>
        </div>
      )}
    </>
  );
};

export default ActionButtonsGrid;
