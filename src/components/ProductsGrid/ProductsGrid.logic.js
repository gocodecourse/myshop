import { DELETE_PRODUCT_URL, SAVE, UPDATE_PRODUCT_URL } from "../../config";

export const deleteProductHandle = async (currentRowIdToDelete) => {
  try {
    const response = await fetch(`${DELETE_PRODUCT_URL}/${currentRowIdToDelete}`, {
      method: "DELETE",
    });
    const answer = await response.json();
    console.log("answer", answer);
  } catch (error) {
    console.error(error);
  }
};

export const handleAction = (action, currentRowId, newRow, oldRow) => {
  if (action === SAVE) {
    delete newRow.id;
    updateProduct(currentRowId, newRow);
    return { ...newRow, id: currentRowId };
  }
  return { ...oldRow };
};

const updateProduct = async (currentRowId, newRow) => {
  try {
    const response = await fetch(`${UPDATE_PRODUCT_URL}/${currentRowId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(newRow),
    });
    const answer = await response.json();
    console.log("answer", answer);
  } catch (error) {
    console.error(error);
  }
};
