import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";

const buttonStyle = { marginLeft: 3, color: "white" };

function TopNavBar() {
  const navigate = useNavigate();

  return (
    <Box
      sx={{
        marginTop: 0.5,
        marginLeft: 5,
      }}
    >
      <Button
        sx={buttonStyle}
        onClick={() => {
          navigate(`/`);
        }}
      >
        Home
      </Button>
      <Button
        sx={buttonStyle}
        onClick={() => {
          navigate(`editProducts`);
        }}
      >
        Edit
      </Button>
      <Button
        sx={buttonStyle}
        onClick={() => {
          navigate(`about`);
        }}
      >
        About
      </Button>
    </Box>
  );
}
export default TopNavBar;
