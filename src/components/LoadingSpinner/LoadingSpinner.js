import React from "react";
import "./LoadingSpinner.css";

const LoadingSpinner = () => {
  return (
    <div className="spinner-examples">
      <div className="example">
        <span className="smooth spinner" />
      </div>
    </div>
  );
};

export default LoadingSpinner;
