import React, { useContext } from "react";

import Product from "../Product/Product";
import "./Products.css";
import StoreContext from "../../StoreContext";
import { ALL_ITEMS, MIN_PRICE_RANGE_INDEX, MAX_PRICE_RANGE_INDEX } from "../../config";
import { Skeleton } from "@mui/material";

const Products = () => {
  const { productsFromServer, error, category, isLoading, priceRange } =
    useContext(StoreContext);
  const productsToFiltered = productsFromServer.filter((product) => {
    if (
      (category.includes(ALL_ITEMS) || category.includes(product.category)) &&
      product.price >= priceRange[MIN_PRICE_RANGE_INDEX] &&
      product.price <= priceRange[MAX_PRICE_RANGE_INDEX]
    ) {
      return product;
    } else {
      return "";
    }
  });

  const productsToDisplay = productsToFiltered.map((product, index) => {
    return (
      <Product
        key={`${product} ${index}`}
        productId={product._id}
        price={product.price}
        imageUrl={product.image}
        title={product.title}
        styleClasses={{
          productCard: "product-card",
          productImg: "product-img",
          productAbstract: "product-abstract",
          buttonsSection: "buttons-section",
        }}
        isProductView={true}
      />
    );
  });

  return (
    <div className="products-section">
      {isLoading ? (
        <div className="loading-products">
          {[...new Array(10)].map((val, index) => (
            <Skeleton
              key={index}
              className="products"
              variant="rectangular"
              width={290}
              height={"30em"}
            />
          ))}
        </div>
      ) : error ? (
        <div>Error fetching data</div>
      ) : (
        <section className="products">{productsToDisplay}</section>
      )}
    </div>
  );
};

export default Products;
