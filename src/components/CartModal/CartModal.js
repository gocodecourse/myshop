import React, { useContext } from "react";
import Modal from "@mui/material/Modal";

import StoreContext from "../../StoreContext";
import "./CartModal.css";
import Product from "../Product/Product";

const CartModal = ({ openModal, setOpenModal }) => {
  const { cart } = useContext(StoreContext);
  const totalSum = [...cart.values()].reduce(
    (sum, product) => sum + product.price * product.amount,
    0
  );

  console.log(cart);
  return (
    <Modal open={openModal} onClose={() => setOpenModal(false)}>
      <div className="modal">
        <h1 className="header">Your cart - {totalSum}$</h1>
        <div className="content">
          {[...cart.entries()].map((entry) => {
            const [productId, product] = entry;
            return (
              <Product
                key={`${productId}-${product.title}`}
                productId={productId}
                price={product.price}
                imageUrl={product.imageUrl}
                title={product.title}
                styleClasses={{
                  productCard: "product-card-modal",
                  productImg: "product-img-modal",
                  productInfo: "product-info-modal",
                  buttonsSection: "buttons-section-modal",
                }}
              />
            );
          })}
        </div>
      </div>
    </Modal>
  );
};

export default CartModal;
