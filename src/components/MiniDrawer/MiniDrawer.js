import React from "react";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import IconButton from "@mui/material/IconButton";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import SortIcon from "@mui/icons-material/Sort";
import AttachMoneyIcon from "@mui/icons-material/AttachMoney";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { DRAWER_WIDTH } from "../../config";
import Filter from "../Filter/Filter";
import Sort from "../Sort/Sort";

const openedMixin = (theme) => ({
  marginTop: "4.5em",
  width: DRAWER_WIDTH,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  marginTop: "4.5em",
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== "open" })(
  ({ theme, open }) => ({
    width: DRAWER_WIDTH,
    flexShrink: 0,
    whiteSpace: "nowrap",
    boxSizing: "border-box",
    ...(open && {
      ...openedMixin(theme),
      "& .MuiDrawer-paper": openedMixin(theme),
    }),
    ...(!open && {
      ...closedMixin(theme),
      "& .MuiDrawer-paper": closedMixin(theme),
    }),
  })
);

const iconStyle = {
  width: "100%",
  padding: "1em 0",
  cursor: "pointer",
  transition: "background 0.5s",
  "&:hover": {
    backgroundColor: "rgb(170, 204, 239)",
  },
};

const boxStyle = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center",
};

const MiniDrawer = ({ openDrawer, setOpenDrawer, openModal, setOpenModal }) => {
  return (
    <Drawer variant="permanent" open={openDrawer}>
      <DrawerHeader>
        <IconButton onClick={() => setOpenDrawer(!openDrawer)}>
          {openDrawer && <ChevronLeftIcon />}
        </IconButton>
      </DrawerHeader>
      <Box sx={boxStyle}>
        {openDrawer ? (
          <>
            <Filter />
            <Sort />
          </>
        ) : (
          <>
            <FilterAltIcon sx={iconStyle} onClick={() => setOpenDrawer(!openDrawer)} />
            <AttachMoneyIcon sx={iconStyle} onClick={() => setOpenDrawer(!openDrawer)} />
            <SortIcon sx={iconStyle} onClick={() => setOpenDrawer(!openDrawer)} />
            <ShoppingCartIcon sx={iconStyle} onClick={() => setOpenModal(true)} />
          </>
        )}
      </Box>
    </Drawer>
  );
};

export default MiniDrawer;
