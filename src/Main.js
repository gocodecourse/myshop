import React, { useState } from "react";
import StoreContext from "./StoreContext";
import { Route, Routes } from "react-router-dom";

import { Home, About, ProductView, EditProducts, NotFound } from "./Pages/Index";
import { ALL_PRODUCTS_URL, ALL_ITEMS } from "./config";
import { useFetch } from "./hooks/useFetch";
import TopAppBar from "./components/TopAppBar/TopAppBar";

function Main() {
  const { data: productsFromServer, isLoading, error } = useFetch(ALL_PRODUCTS_URL);
  const [category, setCategory] = useState([ALL_ITEMS]);
  const [cart, setCart] = useState(new Map());
  const [priceRange, setPriceRange] = useState([0, 500]);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [chosenCategory, setChosenCategory] = useState([ALL_ITEMS]);
  const storeValues = {
    productsFromServer,
    setCategory,
    category,
    isLoading,
    error,
    cart,
    setCart,
    priceRange,
    setPriceRange,
    openDrawer,
    setOpenDrawer,
    chosenCategory,
    setChosenCategory,
  };

  return (
    <StoreContext.Provider value={storeValues}>
      <TopAppBar openDrawer={openDrawer} setOpenDrawer={setOpenDrawer}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="about" element={<About />} />
          <Route path="product/:productId" element={<ProductView />} />
          <Route path="editProducts" element={<EditProducts />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </TopAppBar>
    </StoreContext.Provider>
  );
}

export default Main;
