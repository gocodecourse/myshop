import { useState, useEffect } from "react";

export const useFetch = (url) => {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const getDataFromServer = async () => {
      try {
        setIsLoading(true);
        const response = await fetch(url);
        const answer = await response.json();
        setData(answer);
        setIsLoading(false);
      } catch (error) {
        setError(error);
        setIsLoading(false);
      }
    };
    getDataFromServer();
  }, []);

  return { data, isLoading, error };
};
