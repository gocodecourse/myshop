import { BASE_SERVER_URL } from "./services/service.js";

export const ALL_PRODUCTS_URL = `${BASE_SERVER_URL}/api/products/getAllProducts`;
export const PRODUCT_ID_URL = `${BASE_SERVER_URL}/api/products/getProduct`;
export const UPDATE_PRODUCT_URL = `${BASE_SERVER_URL}/api/products/updateProduct`;
export const DELETE_PRODUCT_URL = `${BASE_SERVER_URL}/api/products/deleteProduct`;

export const ALL_CATEGORIES_URL = "http://localhost:8000/api/services/getAllCategories";

export const SORT_OPTIONS = [
  "Featured",
  "Best Selling",
  "Alphabetically, A-Z",
  "Alphabetically, Z-A",
  "Price, low to high",
  "Price, high to low",
  "Date, new to old",
  "Date, old to new",
];

export const ALL_ITEMS = "All items";
export const MIN_DISTANCE = 10;
export const DRAWER_WIDTH = 280;
export const MIN_PRICE_RANGE_INDEX = 0;
export const MAX_PRICE_RANGE_INDEX = 1;
export const CANCEL = 0;
export const SAVE = 1;
